//! Simple Cowell's integrator.
//!
//! This simply takes the forces applied to the satellite at each timestemp and integrate them.

use crate::body::OrbitalBody;
use crate::frames::InertialReferenceFrame;
use crate::orbit::PVOrbit;
use crate::{Time, Vec3};

pub struct CowellIntegrator<'a> {
    pub bodies: Vec<OrbitalBody<'a>>,
}

impl<'a> CowellIntegrator<'a> {
    pub fn integrate<'f, F>(
        &self,
        mut orbit: PVOrbit<'f, F>,
        to: Time,
        step_nanos: i64,
    ) -> PVOrbit<'f, F>
    where
        F: InertialReferenceFrame + ?Sized,
    {
        let step_seconds = step_nanos as f64 / 1_000_000_000.0;

        while orbit.time < to {
            // Add up all the forces
            let mut total_accel = Vec3::ZERO;

            for body in &self.bodies {
                // Basic newtonian acceleration equation
                // See Fundamentals of Astrodynamics (1971), Bate et al, 1.3-4 pg 14
                let pv = body.pv_at_time(orbit.frame, orbit.time);
                let r = orbit.pv.pos - pv.pos;
                total_accel += -body.mu / r.length().powf(3.) * r;
            }

            // Integrate the acceleration into the velocity
            orbit.pv.vel += total_accel * step_seconds;

            // Integrate the velocity into the position
            orbit.pv.pos += orbit.pv.vel * step_seconds;

            // Advance time
            orbit.time.offset += step_nanos;
        }

        orbit
    }
}
