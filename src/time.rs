use std::fmt::{Debug, Display, Formatter};

/// A timestamp in the J2000 time system.
///
/// This is stored as a number of nanoseconds since the J2000 epoch. It can store dates
/// in the past with negative values.
///
/// Note this is not in terrestrial time - this is a completely fixed timescale, over time
/// it will drift out of sync with UTC.
#[derive(Ord, PartialOrd, Eq, PartialEq)]
pub struct Time {
    pub offset: i64,
}

impl Time {
    const SECOND: i64 = 1_000_000_000;
}

impl Clone for Time {
    fn clone(&self) -> Self {
        Self {
            offset: self.offset,
        }
    }
}

// Since it's just a i64, copying is cheaper than passing references (certainly on 64-bit systems,
// and probably on 32-bit ones too).
impl Copy for Time {}

impl Display for Time {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let total_seconds = self.offset / Time::SECOND;
        let (seconds, minutes) = (total_seconds % 60, total_seconds / 60);
        let (minutes, hours) = (minutes % 60, minutes / 60);
        let (hours, days) = (hours % 24, hours / 24);
        let (days, years) = (days % 365, days / 365);

        let fractional_seconds = (self.offset % Time::SECOND) as f32 / Time::SECOND as f32;
        let real_seconds = seconds as f32 + fractional_seconds;

        // Don't print months currently since we'd have to calculate it and that's a pain
        write!(
            f,
            "TT {:03}-{:04} {:02}:{:02}:{:02.6}",
            days, years, hours, minutes, real_seconds
        )
    }
}

impl Debug for Time {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt(self, f)
    }
}
