//! The definitions for the basic planet frames noted in frames.rs
//!
//! These are taken from https://ssd.jpl.nasa.gov/planets/approx_pos.html for the 1800-2050AD range.

use crate::frames::{InertialReferenceFrame, InertialReferenceFrameRef, ReferenceFrame};
use crate::orbit::{KeplerianAnomaly, KeplerianOrbit, KeplerianTimelessFields};
use std::any::Any;
use std::fmt::Debug;
use std::fmt::Formatter;
use std::sync::Arc;

pub fn create_rough_tree() -> Arc<Box<dyn InertialReferenceFrame>> {
    #[allow(clippy::too_many_arguments)]
    fn build_frame(
        name: &'static str,
        parent: InertialReferenceFrameRef,
        a: f64,
        e: f64,
        i: f64,
        mean_longitude_j2k: f64,
        long_of_peri: f64,
        lan: f64,
    ) -> InertialReferenceFrameRef {
        // Details from the approx_pos page as linked in the module doc
        let arg_of_peri = long_of_peri - lan;
        let mean_anom_j2k = mean_longitude_j2k - long_of_peri;

        let orbit = KeplerianOrbit::new(
            parent,
            KeplerianTimelessFields {
                a,
                e,
                i,
                lan,
                aop: arg_of_peri,
            },
            KeplerianAnomaly::Mean(mean_anom_j2k),
        );

        Arc::new(Box::new(KeplerianBodyFrame {
            name: String::from(name),
            orbit,
            non_inertial_children: vec![],
            inertial_children: vec![],
        }))
    }

    let root: Arc<Box<dyn InertialReferenceFrame>> = Arc::new(Box::new(RootFrame {
        non_inertial_children: vec![],
        inertial_children: vec![],
    }));

    #[rustfmt::skip] let mercury = build_frame("Mercury", root.clone(), 0.38709927, 0.20563593, 7.00497902, 252.25032350, 77.45779628, 48.33076593); // Mercury
    #[rustfmt::skip] let venus   = build_frame("Venus",   root.clone(), 0.72333566, 0.00677672, 3.39467605, 181.97909950, 131.60246718, 76.67984255); // Venus
    #[rustfmt::skip] let em_bary = build_frame("EM Bary", root.clone(), 1.00000261, 0.01671123, -0.00001531, 100.46457166, 102.93768193, 0.0)       ; // EM Bary
    #[rustfmt::skip] let mars    = build_frame("Mars",    root.clone(), 1.52371034, 0.09339410, 1.84969142, -4.55343205, -23.94362959, 49.55953891); // Mars
    #[rustfmt::skip] let jupiter = build_frame("Jupiter", root.clone(), 5.20288700, 0.04838624, 1.30439695, 34.39644051, 14.72847983, 100.47390909); // Jupiter
    #[rustfmt::skip] let saturn  = build_frame("Saturn",  root.clone(), 9.53667594, 0.05386179, 2.48599187, 49.95424423, 92.59887831, 113.66242448); // Saturn
    #[rustfmt::skip] let uranus  = build_frame("Uranus",  root.clone(), 19.18916464, 0.04725744, 0.77263783, 313.23810451, 170.95427630, 74.01692503); // Uranus
    #[rustfmt::skip] let neptune = build_frame("Neptune", root.clone(), 30.06992276, 0.00859048, 1.77004347, -55.12002969, 44.96476227, 131.78422574); // Neptune

    let children = &root
        .as_any()
        .downcast_ref::<RootFrame>()
        .unwrap()
        .inertial_children;

    // This hasn't escaped to another thread yet, so aliasing aside (which there's precious little
    // documentation for with Rust, but it should be fine since there aren't interleaved reads).
    let children = unsafe {
        let children = children as *const Vec<InertialReferenceFrameRef>;
        (children as *mut Vec<InertialReferenceFrameRef>)
            .as_mut()
            .unwrap()
    };

    children.push(mercury);
    children.push(venus);
    children.push(em_bary);
    children.push(mars);
    children.push(jupiter);
    children.push(saturn);
    children.push(uranus);
    children.push(neptune);

    root
}

pub struct RootFrame {
    non_inertial_children: Vec<Arc<Box<dyn ReferenceFrame>>>,
    inertial_children: Vec<Arc<Box<dyn InertialReferenceFrame>>>,
}

impl Debug for RootFrame {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "RootFrame")
    }
}

impl ReferenceFrame for RootFrame {
    fn get_name(&self) -> &str {
        "root"
    }

    fn as_inertial(&self) -> Option<&dyn InertialReferenceFrame> {
        Some(self)
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

impl InertialReferenceFrame for RootFrame {
    fn get_inertial_child_frames(&self) -> &[Arc<Box<dyn InertialReferenceFrame>>] {
        &self.inertial_children
    }

    fn get_non_inertial_child_frames(&self) -> &[Arc<Box<dyn ReferenceFrame>>] {
        &self.non_inertial_children
    }

    fn as_ref_frame(&self) -> &dyn ReferenceFrame {
        self
    }
}

#[derive(Debug)]
pub struct KeplerianBodyFrame {
    pub name: String,
    pub orbit: KeplerianOrbit,
    pub non_inertial_children: Vec<Arc<Box<dyn ReferenceFrame>>>,
    pub inertial_children: Vec<Arc<Box<dyn InertialReferenceFrame>>>,
}

impl ReferenceFrame for KeplerianBodyFrame {
    fn get_name(&self) -> &str {
        &self.name
    }

    fn as_inertial(&self) -> Option<&dyn InertialReferenceFrame> {
        Some(self)
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

impl InertialReferenceFrame for KeplerianBodyFrame {
    fn get_inertial_child_frames(&self) -> &[Arc<Box<dyn InertialReferenceFrame>>] {
        &self.inertial_children
    }

    fn get_non_inertial_child_frames(&self) -> &[Arc<Box<dyn ReferenceFrame>>] {
        &self.non_inertial_children
    }

    fn as_ref_frame(&self) -> &dyn ReferenceFrame {
        self
    }
}
