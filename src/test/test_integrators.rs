use std::sync::Arc;

use crate::body::OrbitalBody;
use crate::orbit::PVOrbit;
use crate::{Time, Vec3, PV};

#[test]
fn test_cowell_basic() {
    let tree = crate::rough_frames::create_rough_tree();
    let mars = tree
        .get_child_by_name("Mars")
        .unwrap()
        .as_inertial()
        .unwrap();

    let cowell = crate::integrators::cowell::CowellIntegrator {
        bodies: vec![OrbitalBody {
            name: "mars".to_string(),
            mu: 42828370000000.0, // From Wikipedia
            centric_frame: mars,
        }],
    };

    let start = Time { offset: 0 };
    let end = Time {
        offset: 3600 * 1_000_000_000i64,
    };

    let initial = PVOrbit {
        frame: mars,
        time: start,
        pv: PV {
            pos: Vec3::new(10_000_000., 0., 0.),
            vel: Vec3::new(0., 8000., 0.),
        },
    };

    let output = cowell.integrate(initial, end, 1_000_000);

    let orekit_pv = PV {
        pos: Vec3::new(8613819.937933112, 2.8034522381433096E7, 0.0),
        vel: Vec3::new(-511.74327150225383, 7621.882309427537, 0.0),
    };
    let pos_acc = orekit_pv.pos.distance(output.pv.pos);
    let vel_acc = orekit_pv.vel.distance(output.pv.vel);
    more_asserts::assert_ge!(1., pos_acc);
    more_asserts::assert_ge!(0.001, vel_acc);

    println!("End state: {:?}", output.pv);
    println!("Accuracy: {}m/s, {}m/s/s", pos_acc, vel_acc);
}
