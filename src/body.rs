use crate::frames::{InertialReferenceFrame, ReferenceFrame};
use crate::{Time, PV};

pub struct OrbitalBody<'a> {
    pub name: String,

    /// The gravitational constant of this body
    ///
    /// This can indeed be calculated with the universal gravitational constant and the mass, but
    /// various models publish Mu values that we should probably just use directly for sake of
    /// accuracy.
    pub mu: f64,

    pub centric_frame: &'a dyn InertialReferenceFrame,
}

impl<'a> OrbitalBody<'a> {
    pub fn get_orbit_for<Frame>(&self, _frame: &Frame)
    where
        Frame: ReferenceFrame + ?Sized,
    {
        todo!()
    }

    /// Gets the position and velocity of this body at a given point in time.
    ///
    /// This should be very fast, as integrators may call it a lot.
    pub fn pv_at_time<Frame>(&self, frame: &Frame, _time: Time) -> PV
    where
        Frame: ReferenceFrame + ?Sized,
    {
        // If this is our centric frame, it makes things easy
        if let Some(irf) = frame.as_inertial() {
            // FIXME CLEAN THIS UP
            #[warn(clippy::vtable_address_comparisons)]
            if std::ptr::eq(self.centric_frame, irf) {
                return PV::zero();
            }
        }

        todo!()
    }
}
