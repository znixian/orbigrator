pub mod body;
pub mod frames;
pub mod integrators;
pub mod orbit;
pub mod rough_frames;
pub mod satellite;
pub mod time;

pub use glam::DVec3 as Vec3;
pub use satellite::Satellite;
pub use time::Time;

#[cfg(test)]
mod test;

#[derive(Debug)]
pub struct PV {
    pos: Vec3,
    vel: Vec3,
}

impl PV {
    pub fn zero() -> PV {
        PV {
            pos: Vec3::ZERO,
            vel: Vec3::ZERO,
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
