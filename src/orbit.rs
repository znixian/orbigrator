use crate::frames::{InertialReferenceFrame, ReferenceFrame};
use crate::{Time, PV};
use std::fmt::Debug;
use std::sync::Arc;

/// Represents an orbit in some form.
///
/// This is a trait and not a struct since there are many different ways of representing orbits. For
/// example, SGP4 needs positions in TLE format, and when propagating an orbit with Encke's method
/// it is useful to store both the Keplerian and perturbation data in the orbit to avoid having to
/// repeatedly solve Kepler's problem.
///
/// Orbits can be arbitrarily converted between different frames. When repeatedly integrating an
/// orbit it's unwise to convert from the integrator's preferred orbit format and back, to avoid
/// a loss of precision and potentially a severe performance penalty.
///
/// For most orbits, the reference frame must be an inertial one. However, some orbits can be
/// stored in fixed frames (notably PV orbits). This is particularly useful during conversion - for
/// example, one can take a Keplerian orbit, convert it to PV and then transform it into a fixed
/// frame to get a satellite's position in a fixed frame.
pub trait Orbit: Debug {
    fn get_frame(&self) -> &dyn ReferenceFrame;

    fn get_time(&self) -> Time;
}

/// The simplest form of orbit data. This stores the position and velocity of the satellite relative
/// to the parent frame.
#[derive(Debug)]
pub struct PVOrbit<'a, Frame>
where
    Frame: ReferenceFrame + ?Sized,
{
    pub frame: &'a Frame,
    pub time: Time,
    pub pv: PV,
}

impl<'a, Frame> Orbit for PVOrbit<'a, Frame>
where
    Frame: ReferenceFrame,
{
    fn get_frame(&self) -> &dyn ReferenceFrame {
        self.frame
    }

    fn get_time(&self) -> Time {
        self.time
    }
}

/// Basic Keplerian orbit.
///
/// This struct describes an orbit around the origin of the frame.
#[derive(Debug)]
pub struct KeplerianOrbit {
    /// The reference frame this orbit operates in
    pub frame: Arc<Box<dyn InertialReferenceFrame>>,

    /// The fields defining the path this orbit takes around the origin of the frame.
    pub fields: KeplerianTimelessFields,

    /// The 'source of truth' anomaly which defines this orbit
    anomaly: KeplerianAnomaly,
    /// A cache of the computed other anomaly.
    other_anomaly: Option<f64>,
}

/// The non-anomaly fields of a keplerian orbit. These can be reused in other places outside
/// of the Keplerian orbit definition.
#[derive(Debug)]
pub struct KeplerianTimelessFields {
    /// Semi-major axis
    pub a: f64,
    /// Eccentricity
    pub e: f64,
    /// Inclination
    pub i: f64,
    /// Longitude of the ascending node, in radians
    pub lan: f64,
    /// Argument of periapsis, in radians
    pub aop: f64,
}

/// A selection of either the true or mean anomaly which is the most accurately known.
///
/// If we're propagating an orbit for example, we wouldn't want to repeatedly convert between
/// the true and mean anomaly. This system lets us consider a single one of the anomalies as the
/// most accurate, and compute and cache the other as a result of that.
///
/// This is particularly aimed at the scenario where we continually vary the mean anomaly and then
/// convert the orbit to PV parameters; since we can easily convert mean to true anomalies (going
/// in the easy direction through Kepler's equation) we really want to just use that as our source
/// of truth; however other times we may have to process the orbit without needing to compute the
/// mean anomaly.
///
/// Perf note: this is Copy since it's probably 16 bytes after padding - while bigger than a single
/// 64-bit register still probably a lot faster than a cache-hitting read.
#[derive(Debug, Clone, Copy)]
pub enum KeplerianAnomaly {
    True(f64),
    Mean(f64),
}

impl KeplerianOrbit {
    pub fn new(
        frame: Arc<Box<dyn InertialReferenceFrame>>,
        fields: KeplerianTimelessFields,
        anomaly: KeplerianAnomaly,
    ) -> KeplerianOrbit {
        KeplerianOrbit {
            frame,
            fields,
            anomaly,
            other_anomaly: None,
        }
    }

    pub fn primary_anomaly(&self) -> KeplerianAnomaly {
        self.anomaly
    }
}
