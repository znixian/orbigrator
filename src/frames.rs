//! Reference frame system.
//!
//! This supports the definition of arbitrary reference frames which can be converted between
//! and form a hierarchy. This supports both inertial and non-inertial frames.
//!
//! The tree of reference frames might look something like this (note some of these frames are
//! added solely to serve as examples of where they would fit, and aren't currently present):
//!
//! ```text
//! HCI (Heliocentric-inertial)
//! ├ EMBary (Earth-moon barycentre)
//! │ ├ GCI (Geocentric-inertial)
//! │ └ Lunar (Earth moon-centric inertial)
//! │   └ LunarFixed
//! ├ Mars (Mars-centric inertial)
//! │ └ MarsFixed
//! etc
//! ```
//!
//! Note these are not high-accuracy reference frames; they are the approximate values from the
//! JPL SSD website [1]. These will more than suffice for kicking the tyres of this framework, and
//! for use in many applications. However, one should be able to make a tree using high-precision
//! frames if required.
//!
//! Note that inertial frames are always children of other inertial frames. An inertial frame cannot
//! be a child of a non-inertial frame, but the inverse is quite common.
//!
//! Also note that this tree is arranged according to the physical layout of the solar system, not
//! according to how data is derived. This is unlike Orekit (on which a lot of this is conceptually
//! based) where GCRF is the root frame since everything else is derived from it owing to
//! measurement accuracy.
//!
//! \[1] https://ssd.jpl.nasa.gov/planets/approx_pos.html

use std::any::Any;
use std::fmt::Debug;
use std::ops::Deref;
use std::sync::Arc;

pub trait ReferenceFrame: Debug {
    /// Get the non-localised non-pretty name for this reference frame.
    ///
    /// This should not contain spaces or strokes (the latter may be used for building paths to
    /// frames). An example of a reasonable value might be 'iau15mars' or or 'iau15mars-fixed'.
    fn get_name(&self) -> &str;

    /// If this is an inertial frame, get a reference to it in that form. If this is not
    /// an inertial frame, returns None.
    fn as_inertial(&self) -> Option<&dyn InertialReferenceFrame>;

    /// Get this as an Any for down-casting.
    fn as_any(&self) -> &dyn Any;

    // TODO conversion functions
}

pub trait InertialReferenceFrame: ReferenceFrame {
    /// Get a list of all the inertial child frames.
    fn get_inertial_child_frames(&self) -> &[InertialReferenceFrameRef];

    /// Get a list of all the non-inertial child frames.
    fn get_non_inertial_child_frames(&self) -> &[ReferenceFrameRef];

    /// Gets a reference to this frame as a basic (non-inertial) frame.
    fn as_ref_frame(&self) -> &dyn ReferenceFrame;
}

impl dyn InertialReferenceFrame {
    pub fn get_all_children(&self) -> impl Iterator<Item = &dyn ReferenceFrame> {
        let non_inertial_iter = self
            .get_non_inertial_child_frames()
            .iter()
            .map(|i| i.deref().deref());

        let inertial_iter = self
            .get_inertial_child_frames()
            .iter()
            .map(|i| i.as_ref_frame());

        inertial_iter.chain(non_inertial_iter)
    }

    pub fn get_child_by_name(&self, name: &str) -> Option<&dyn ReferenceFrame> {
        self.get_all_children().find(|f| f.get_name() == name)
    }
}

pub type ReferenceFrameRef = Arc<Box<dyn ReferenceFrame>>;
pub type InertialReferenceFrameRef = Arc<Box<dyn InertialReferenceFrame>>;
