/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package validator;

import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.hipparchus.ode.nonstiff.AdaptiveStepsizeIntegrator;
import org.hipparchus.ode.nonstiff.DormandPrince853Integrator;
import org.orekit.bodies.CelestialBody;
import org.orekit.data.DataContext;
import org.orekit.data.DirectoryCrawler;
import org.orekit.frames.Frame;
import org.orekit.orbits.CartesianOrbit;
import org.orekit.orbits.OrbitType;
import org.orekit.propagation.SpacecraftState;
import org.orekit.propagation.numerical.NumericalPropagator;
import org.orekit.time.AbsoluteDate;
import org.orekit.utils.PVCoordinates;

import java.io.File;

public class App {
    public static void main(String[] args) {
        File orekitData = new File("orekit-data-master");
        DataContext.getDefault().getDataProvidersManager().addProvider(new DirectoryCrawler(orekitData));
        CelestialBody mars = DataContext.getDefault().getCelestialBodies().getMars();
        Frame frame = mars.getInertiallyOrientedFrame();

        PVCoordinates initialPV = new PVCoordinates(new Vector3D(10_000_000, 0, 0), new Vector3D(0, 8000, 0));
        CartesianOrbit orbit = new CartesianOrbit(initialPV, frame, AbsoluteDate.J2000_EPOCH, mars.getGM());

        final double dP = 0.001;
        final double minStep = 0.001;
        final double maxStep = 500;
        final double initStep = 60;
        final double[][] tolerance = NumericalPropagator.tolerances(dP, orbit, OrbitType.CARTESIAN);
        AdaptiveStepsizeIntegrator integrator = new DormandPrince853Integrator(minStep, maxStep, tolerance[0], tolerance[1]);

        NumericalPropagator prop = new NumericalPropagator(integrator);
        prop.resetInitialState(new SpacecraftState(orbit));
        prop.setOrbitType(OrbitType.CARTESIAN);
        SpacecraftState finalState = prop.propagate(AbsoluteDate.J2000_EPOCH.shiftedBy(3600));

        System.out.println(finalState);
    }
}
